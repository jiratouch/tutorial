async function migration(app) {
    try {
        await app.dataSources.mysql.autoupdate()
        console.log('Finish update')
        return Promise.resolve();
    } catch (e) {
        console.log('error while update database', e);
        return Promise.reject(e);
    }
}

module.exports = migration;