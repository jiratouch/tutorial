module.exports = {
    mysql: {
        url: process.env.DATABASE_URL,
        name: "mysql",
        connector: "mysql"
    }
}