'use strict';
const app = require('../../server/server')

module.exports = function(Transaction) {
    Transaction.observe('after save', async ctx => {
        try {
            const {
                Account,
            } = app.models;
            if (!ctx.instance) { // skip if not have instance
                return Promise.resolve();
            }
            const transaction = ctx.instance;

            const account = await Account.findById(transaction.accountId)
            // calculate total
            await account.recalculateTotal(transaction)
            return Promise.resolve();
        }
        catch (e) {
            return Promise.reject(e);
        }
    })
};
