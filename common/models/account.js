'use strict';

module.exports = function(Account) {
    Account.prototype.recalculateTotal = async function(transaction) {
        try {
            const account = await Account.findById(transaction.accountId);
            console.log('account', account)
            // totalIncome = tranSaction.reduce((totalIncome, currentItem) => {
            //     return totalIncome + (currentItem.Deposit - currentItem.Withdraw)
            // }, 0);
            let amountToProcess = 0;
            if(transaction.Deposit) {
                amountToProcess = transaction.Deposit;
            }
            else if(transaction.Withdraw) {
                amountToProcess = (-transaction.Withdraw)
            }
            account.Amount += amountToProcess;


            return account.save();
        } catch (e) {
            console.debug('error while recalculate total price', e);
            return Promise.reject(e);
        }
    }
};
